function layThongTinTuForm() {
  let maSV = document.getElementById("txtMaSV").value.trim();
  let tenSV = document.getElementById("txtTenSV").value.trim();
  let email = document.getElementById("txtEmail").value.trim();
  let matkhau = document.getElementById("txtPass").value.trim();
  let diemLy = document.getElementById("txtDiemLy").value.trim();
  let diemToan = document.getElementById("txtDiemToan").value.trim();
  let diemHoa = document.getElementById("txtDiemHoa").value.trim();

  // tạo object từ thông tin lấy từ form
  var sv = new SinhVien(maSV, tenSV, email, matkhau, diemLy, diemToan, diemHoa);
  return sv;
}

function renderDssv(list) {
  // render danh sách
  // contentHTML là 1 chuỗi chứa các thẻ tr sau này sẽ innerHTML vào thẻ tbody
  var contentHTML = "";

  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];

    var contentTr = `<tr>
    <td>${currentSv.ma}</td>
    <td>${currentSv.ten}</td>
    <td>${currentSv.email}</td>
    <td>0</td>
    <td>
      <button class="btn btn-danger" onclick = "xoaSV('${currentSv.ma}')">Xoa</button>
      <button class="btn btn-primary" onclick = "suaSV('${currentSv.ma}')">Sua</button>
    </td>
    </tr>`;

    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("txtMaSV").disabled = false;
}
