// Pending (Chờ Response), Reject (Thất bại), Resolve (Thành công)

// axios là bất đồng bộ

const BASE_URL = "https://633ec0420dbc3309f3bc527c.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};

var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var fetchDssvService = function () {
  batLoading();

  // lấy danh sách sinh viên service
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      renderDanhSachSinhVien(response.data);
      tatLoading();
    })
    .catch(function (error) {
      console.log("error: ", error);
      tatLoading();
    });
};
// chạy lần đầu khi load trang
fetchDssvService();

// render danh sách sinh viên
var renderDanhSachSinhVien = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    var tinhDTB = ((sv.ly * 1 + sv.toan * 1 + sv.hoa * 1) / 3).toFixed(2);
    contentHTML += `
        <tr>      
            <td>${sv.ma}</td>
            <td>${sv.ten}</td>
            <td>${sv.email}</td>
            <td>${tinhDTB}</td>
            <td>          
                <button onclick="xoaSv('${sv.ma}')" class="btn btn-danger">Xóa</button>
                <button onclick="layThongTinChiTietSv(${sv.ma})" class="btn btn-primary">Sửa</button>
            </td>
        </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xoa Sv
function xoaSv(idSv) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      fetchDssvService();
      // thông báo thành công
      Swal.fire("Xóa thành công");
      console.log("res: ", res);
    })

    .catch(function (err) {
      Swal.fire("Xóa thất bại");
      console.log("err: ", err);
    });
}

// them sinh vien
var themSV = function () {
  batLoading();
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (response) {
      tatLoading();
      console.log("response: ", response);
      fetchDssvService();
      Swal.fire("Thêm thành công");
    })
    .catch(function (error) {
      Swal.fire("Thêm thất bại");
      console.log("error: ", error);
    });
};

var layThongTinChiTietSv = function (idSv) {
  batLoading();
  document.getElementById("txtMaSV").disabled = true;

  // way 1
  // axios({
  //   url: `${BASE_URL}/sv/${idSv}`,
  //   method: "PUT",
  // })
  //   .then(function (res) {
  //     tatLoading();
  //     showThongTinLenForm(res.data);
  //     console.log("res: ", res);
  //   })
  //   .catch(function (err) {
  //     console.log("err: ", err);
  //   });

  //way 2
  axios
    .put(`${BASE_URL}/sv/${idSv}`)
    .then(function (res) {
      tatLoading();
      showThongTinLenForm(res.data);
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};

var capNhatSv = function () {
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      console.log("res: ", res);
      fetchDssvService();
      Swal.fire("Cập nhật thành công");
    })
    .catch(function (err) {
      console.log("err: ", err);
      Swal.fire("Cập nhật thất bại");
    });
  resetForm();
};
