function SinhVien(_ma, _ten, _email, _matKhau, _diemLy, _diemToan, _diemHoa) {
  this.ma = _ma;
  this.ten = _ten;
  this.email = _email;
  this.matKhau = _matKhau;
  this.ly = _diemLy;
  this.toan = _diemToan;
  this.hoa = _diemHoa;

  this.tinhDTB = () => {
    return (this.ly * 1 + this.toan * 1 + this.hoa * 1) / 3;
  };
}
