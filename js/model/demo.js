// demo
var cat1 = {
    catName: "Alice"
}

var cat2 = {
    catName: "bob",
};

// tạo lớp đối tượng
// khuôn bánh
function Cat(_name, _age){
    this.name = _name;
    this.age = _age;
    this.talk = function () {
        console.log("gau gau");
    }
}

// cat3 ~ bánh được tạo từ khuôn
var cat3 = new Cat("num", 3);
console.log('cat3: ', cat3);
cat3.talk();
